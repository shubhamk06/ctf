# Miscellaneous 3 - ilovebees

## Points
    - 200
---
## Description
>
    I stumbled on to this strange website. It seems like a website made by a flower enthusiast, but it appears to have been taken over by someone... or something.

    Can you figure out what it's trying to tell us?

    https://static.icec.tf/iloveflowers/
>
---
## Solve

After poking arround on the Website I noticed that the only noticable thing was the `favicon` this was a gif icon.

I then downloaded the gif to my PC. Once downloaded i ran `strings -n 8 favicon.gif`.

I then got this result:
![IceCTF Misc](images/icectf2018/IceCtfMisc3.PNG "Misc3")

As there was no Flag within i then opend it within Gimp. This didnt help aswell.

After a lot of different things it then stumbeld apond a command `convert favicon.gif favicon.png`.

This then split each Frame of the GIF into a seperate PNG image.

Once it then has completed this task i then ran the command `strings * favicon.png | grep "Ice"`.

I then got this result:
![IceCTF Misc](images/icectf2018/IceCtfMisc3a.PNG "Misc3")

and there I found the flag.

The Flag is:
`IceCTF{MY_FAVORITE_ICON}`