# Cryptography 3 - Ancient Foreign Communications

## Points
    - 300
---
## Description
>
    We got word from a friend of ours lost in the depths of the Andorran jungles! Help us figure out what he is trying to tell us before its too late!

Note: The flag here is non-standard, in the result you should end up with some words! The flag is IceCTF{<words, lowercase, including spaces>}
>
---
## Solve

At the start of the cryptic Challenge we were given a bunch of `HEX` values:

>
E2 A8 85 5D 5D E2 8C 9E E2 8C 9E E2 8C 9F 5B E2 A8 86 5D E2 8C 9F 5D 5D 5D E2 A8 86 E2 A8 86 E2 A8 86 E2 8C 9C 5B 5B 5B E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9E E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9D E2 A8 86 E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9E E2 8C 9E E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9F E2 8C 9D E2 8C 9D E2 A8 85 E2 A8 85 E2 8C 9E E2 8C 9E E2 A8 86 5B 5D 5D 5D E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9D 5D 5D E2 8C 9F 5B 5B 5B E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9F E2 8C 9D E2 8C 9D E2 8C 9D E2 8C 9D 5D 5D 5D E2 8C 9E E2 8C 9E E2 8C 9E E2 8C 9D E2 8C 9D E2 8C 9D E2 A8 86 5D E2 8C 9E E2 8C 9E
>

The first thing I did was to transform the `HEX` to `TEXT` which resulted into:
( the webiste i used to transelate it was: https://cryptii.com/hex-to-text)
>
⨅]]⌞⌞⌟[⨆]⌟]]]⨆⨆⨆⌜[[[⌝⌝⌝⌞⌝⌝⌝⌝⨆⌝⌝⌝⌞⌞⌝⌝⌝⌝⌟⌝⌝⨅⨅⌞⌞⨆[]]]⌝⌝⌝⌝]]⌟[[[⌝⌝⌝⌝⌟⌝⌝⌝⌝]]]⌞⌞⌞⌝⌝⌝⨆]⌞⌞
>

After a verrrry looooooooooong time i then discovered that it was a `T9 `encoding ( like a old mobile Phone with a numberpad ). When i managed to get the different Letters it correlated to:

![IceCTF Crypto3](images/icectf2018/IceCrypto1.PNG "Crypto3")

>
t h e _ m a g _ i c w o r d s a r e s _ q u e a m i s h _ o s _ s i f r a g e
(the magic words are squeamish ossifrage)
>

The solution to this challenge was the Person who invented the Ciphertext RSA in the year 1977 ( https://en.wikipedia.org/wiki/The_Magic_Words_are_Squeamish_Ossifrage )

The Flag is:
`IceCTF{squeamish ossifrage}`
