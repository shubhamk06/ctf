# Web 2 - Lights out

## Points
    - 75
---
## Description
>
    Help! We’re scared of the dark!
>
---
## Solve

For this Challenge we were given a black screen website. 


![IceCTF Web2](images/icectf2018/Iceweb2index.PNG "Web2")

I inspected a the HTMl code within there is saw :

>
<div class="clearfix">
    <i data-hide="true"></i>
        <strong data-show="true">
            <small></small>
        </strong>
    <small></small>
</div>
>

As this didn't given realy give me nay real hints i then looked at the `style.css`. 

Whithin there there were sections which started with `Pseudo`.

In those different blocks it had a `content`section :
`
>
    *content: "Ic";
    *content: "eCTF{";
    *content: "the_lights}";
    *content: "styles";
    *content: "turned";
    *content: "_";
>
`

After getting all the different bits for the flag, it then needed to be rearanged.

After rearanging it the Flag is:
`IceCTF{styles_turned_the_lights}`