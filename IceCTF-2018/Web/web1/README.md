# Web 1 - Toke

## Points
    - 50
---
## Description
>
    We’ve relaunched our famous website, Toke! Hopefully no one will hack it again and take it down like the last time.
>
---
## Solve

At the beginning when we accesed the Link we were given a Website. On there in the center there was a button, which seemed not to be doing anything.


![IceCTF Web1](images/icectf2018/web1index.PNG "Web1")

After scrolling through the whole page and didn't find anything on it, I then proceded to look at the `/robots.txt`. This File is used for search robots to know which files they should not visit. 

[robots.txt reference ]( http://www.webconfs.com/what-is-robots-txt-article-12.php)

When looking at the `/robots.txt` it displays us :

`
User-agent: *
Disallow: /secret_xhrznylhiubjcdfpzfvejlnth.html
`

I then navigate to the following page:
>
    https://static.icec.tf/toke/secret_xhrznylhiubjcdfpzfvejlnth.html
>

On that page we then got the Flag displayed in clear text.

The Flag is:
`IceCTF{what_are_these_robots_doing_here}`